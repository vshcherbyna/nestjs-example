import {Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne} from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Group } from '../../groups/entities/group.entity';
import { Course } from '../../courses/entities/course.entity';
import {ApiProperty} from "@nestjs/swagger";

@Entity({ name: 'students' })
export class Student extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  @ApiProperty()
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  @ApiProperty()
  surname: string;

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  @ApiProperty()
  email: string;

  @Column({
    type: 'numeric',
    nullable: false,
  })
  @ApiProperty()
  age: number;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'image_path',
  })
  @ApiProperty()
  imagePath: string;

  @Column({
    type: 'integer',
    nullable: true,
    name: 'group_id',
  })
  @ApiProperty()
  groupId: number;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: true,
    eager: false,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'group_id' })
  @ApiProperty()
  group: Group;


  @ManyToMany(
    () => Course,
    (course) => course.students,
    { onDelete: 'NO ACTION', onUpdate: 'NO ACTION' },
  )
  @JoinTable({
    name: 'student_course',
    joinColumn: {
      name: 'student_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'course_id',
      referencedColumnName: 'id',
    },
  })
  courses?: Course[];
}
