import { Injectable } from '@nestjs/common';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import {InjectRepository} from "@nestjs/typeorm";
import {FindOptionsOrder, Repository} from "typeorm";
import {Student} from "./entities/student.entity";
import {QueryFilterDto} from "../../application/dto/query.filter.dto";

@Injectable()
export class StudentsService {

  constructor(
    @InjectRepository(Student)
    private readonly studentRepository: Repository<Student>
  ) {}

  create(createStudentDto: CreateStudentDto) {
    return this.studentRepository.save(createStudentDto);
  }

  findAll(queryFilter: QueryFilterDto) {
    return this.studentRepository.find({
      order: {
        [queryFilter.sortField]: queryFilter.sortOrder
      }
    });
  }

  findOne(id: string) {
    return this.studentRepository.findOneBy({ id });
  }

  update(id: number, updateStudentDto: UpdateStudentDto) {
    return this.studentRepository.update(id, updateStudentDto);
  }

  remove(id: number) {
    return this.studentRepository.delete(id);
  }

}
