import { Module } from '@nestjs/common';
import {StudentManagerService} from "./student-manager.service";
import {StudentsModule} from "../students/students.module";
import {CoursesModule} from "../courses/courses.module";

@Module({
  imports: [StudentsModule, CoursesModule],
  providers: [StudentManagerService],
  exports: [StudentManagerService]
})
export class StudentManagerModule {}
