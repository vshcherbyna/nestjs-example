import { Injectable } from '@nestjs/common';
import {CoursesService} from "../courses/courses.service";
import {StudentsService} from "../students/students.service";

@Injectable()
// This service uses studentsService and coursesService and has methods calculateCourseStatisticsForStudent, calculateStudentStatisticsForCourse
// This approach can resolve circular dependency problem ->
// For method calculateCourseStatisticsForStudent we need (coursesService and studentsService). This method should be in StudentsService, because logic is related to student
// And for method calculateStudentStatisticsForCourse we need (coursesService and studentsService). This method should be in CoursesService, because logic is related to course
// So in this case we will have a circular dependency because StudentsModule will use CoursesModule and CoursesModule will use StudentsModule.
// To prevent this issue it's better to create 3-rd service (StudentManagerService) and move all logic that need both services to it. And then use it in the code
// Also, all services that are working with database should work only with database and shouldn't have other services as dependency
export class StudentManagerService {

  constructor(
    private readonly studentsService: StudentsService,
    private readonly coursesService: CoursesService,
  ) {}

  public async calculateCourseStatisticsForStudent(studentId: string) {
    const student = await this.studentsService.findOne(studentId);
    const courses = await this.coursesService.findAll();
    /* Some extra logic */
  }

  public async calculateStudentStatisticsForCourse(studentId: string) {
    const student = await this.studentsService.findOne(studentId);
    const courses = await this.coursesService.findAll();
    /* Some extra logic */
  }

}
