import { PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import {ApiProperty} from "@nestjs/swagger";

export abstract class CoreEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  @ApiProperty()
  public id: string;

  @CreateDateColumn({ type: 'timestamp with time zone', name: 'created_at' })
  @ApiProperty()
  public createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone', name: 'updated_at' })
  @ApiProperty()
  public updatedAt: Date;
}
