import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppService } from './app.service';
import { GroupsModule } from '../services/groups/groups.module';
import { CoursesModule } from '../services/courses/courses.module';
import { StudentsModule } from '../services/students/students.module';
import { CoursesControllerModule } from '../controllers/courses/courses.controller.module';
import { GroupsControllerModule } from '../controllers/groups/groups.controller.module';
import { StudentsControllerModule } from '../controllers/students/students.controller.module';
import { typeOrmAsyncConfig } from '../configs/database/typeorm-config';
import { ConfigModule } from '../configs/config.module';
import {AuthControllerModule} from "../controllers/auth/auth.controller.module";
import {UsersModule} from "../services/users/users.module";
import {AuthModule} from "../services/auth/auth.module";
import {ResetTokenModule} from "../services/reset-token/reset-token.module";
import {UsersControllerModule} from "../controllers/users/users.controller.module";

@Module({
  imports: [
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    ConfigModule,
    GroupsModule,
    CoursesModule,
    StudentsModule,
    CoursesControllerModule,
    GroupsControllerModule,
    StudentsControllerModule,
    AuthControllerModule,
    UsersModule,
    AuthModule,
    ResetTokenModule,
    UsersControllerModule,
  ],
  providers: [AppService],
})
export class AppModule {}
