import { Injectable } from '@nestjs/common';
import { StudentsService } from '../../services/students/students.service';
import { CreateStudentDto } from '../../services/students/dto/create-student.dto';
import { UpdateStudentDto } from '../../services/students/dto/update-student.dto';
import {QueryFilterDto} from "../../application/dto/query.filter.dto";

@Injectable()
export class StudentsControllerService {

  constructor(private readonly studentsService: StudentsService) {
  }

  create(createStudentDto: CreateStudentDto) {
    return this.studentsService.create(createStudentDto);
  }

  findAll(queryFilter: QueryFilterDto) {
    return this.studentsService.findAll(queryFilter);
  }

  findOne(id: string) {
    return this.studentsService.findOne(id);
  }

  update(id: number, updateStudentDto: UpdateStudentDto) {
    return this.studentsService.update(id, updateStudentDto);
  }

  remove(id: number) {
    return this.studentsService.remove(id);
  }
}
