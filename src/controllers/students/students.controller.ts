import {Controller, Get, Post, Body, Patch, Param, Delete, Query} from '@nestjs/common';
import { StudentsControllerService } from './students.controller.service';
import { CreateStudentDto } from '../../services/students/dto/create-student.dto';
import { UpdateStudentDto } from '../../services/students/dto/update-student.dto';
import {QueryFilterDto} from "../../application/dto/query.filter.dto";

@Controller('students')
export class StudentsController {
  constructor(private readonly studentsControllerService: StudentsControllerService) {}

  @Post()
  create(@Body() createStudentDto: CreateStudentDto) {
    return this.studentsControllerService.create(createStudentDto);
  }

  @Get()
  findAll(@Query() queryFilter: QueryFilterDto) {
    return this.studentsControllerService.findAll(queryFilter);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.studentsControllerService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateStudentDto: UpdateStudentDto) {
    return this.studentsControllerService.update(+id, updateStudentDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.studentsControllerService.remove(+id);
  }
}
