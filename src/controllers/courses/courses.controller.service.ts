import { Injectable } from '@nestjs/common';
import { CoursesService } from '../../services/courses/courses.service';
import { CreateCourseDto } from '../../services/courses/dto/create-course.dto';
import { UpdateCourseDto } from '../../services/courses/dto/update-course.dto';

@Injectable()
export class CoursesControllerService {

  constructor(
    private readonly coursesService: CoursesService,
  ) {}

  create(createCourseDto: CreateCourseDto) {
    return this.coursesService.create(createCourseDto);
  }

  findAll() {
    return this.coursesService.findAll();
  }

  findOne(id: string) {
    return this.coursesService.findOne(id);
  }

  update(id: number, updateCourseDto: UpdateCourseDto) {
    return this.coursesService.update(id, updateCourseDto);
  }

  remove(id: number) {
    return this.coursesService.remove(id);
  }
}
