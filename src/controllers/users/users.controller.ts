import {Controller, Get, UseGuards} from '@nestjs/common';
import {UsersControllerService} from "./users.controller.service";
import {CurrentUser} from "../../services/auth/decorators/current.user.decorator";
import {User} from "../../services/users/users.service";
import {AuthGuard} from "../../services/auth/guards/auth.guard";
import {ApiBearerAuth} from "@nestjs/swagger";

@UseGuards(AuthGuard)
@Controller('')
@ApiBearerAuth('jwt')
export class UsersController {
  constructor(private readonly usersControllerService: UsersControllerService) {}

  @Get('/me')
  public async findMe(@CurrentUser() loggedUser: User) {
    return await this.usersControllerService.findCurrentUser(loggedUser.userId)
  }

}
