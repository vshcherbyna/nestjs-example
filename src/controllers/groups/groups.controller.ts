import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { GroupsControllerService } from './groups.controller.service';
import { CreateGroupDto } from '../../services/groups/dto/create-group.dto';
import { UpdateGroupDto } from '../../services/groups/dto/update-group.dto';

@Controller('groups')
export class GroupsController {
  constructor(private readonly groupsControllerService: GroupsControllerService) {}

  @Post()
  create(@Body() createGroupDto: CreateGroupDto) {
    return this.groupsControllerService.create(createGroupDto);
  }

  @Get()
  findAll() {
    return this.groupsControllerService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.groupsControllerService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateGroupDto: UpdateGroupDto) {
    return this.groupsControllerService.update(+id, updateGroupDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.groupsControllerService.remove(+id);
  }
}
