import { Injectable } from '@nestjs/common';
import {GroupsService} from '../../services/groups/groups.service';
import {CreateGroupDto} from '../../services/groups/dto/create-group.dto';
import {UpdateGroupDto} from '../../services/groups/dto/update-group.dto';

@Injectable()
export class GroupsControllerService {

  constructor(private readonly groupsService: GroupsService) {}

  create(createGroupDto: CreateGroupDto) {
    return this.groupsService.create(createGroupDto);
  }

  findAll() {
    return this.groupsService.findAll();
  }

  findOne(id: string) {
    return this.groupsService.findOne(id);
  }

  update(id: number, updateGroupDto: UpdateGroupDto) {
    return this.groupsService.update(id, updateGroupDto);
  }

  remove(id: number) {
    return this.groupsService.remove(id);
  }
}
